# Unofficial Python SatNOGS API

This project should not exist anymore. It was started when satnogs-network and satnogs-db did not have
any official API client. Now they do, but they are not ready to use yet (as of 2022 their state can be 
described as a combination of broken and not documented).

PLEASE DO NOT CONTRIBUTE TO THIS PROJECT (unless it is a trivial bugfix). Invesst into bringing the
official API client live.

The new (old) upstream repository is <https://gitlab.com/kerel-fs/python-satnogs-api>.
At some point in the past the project was migrated to
<https://gitlab.com/librespacefoundation/satnogs/python-satnogs-api>, but this fork was archived again.
See also the history section.

## Installation

Install with pip using git transport:
```
pip install git+https://gitlab.com/librespacefoundation/satnogs/python-satnogs-api@master
```

## Development
```
pip install -e .
```

## Usage

Functions:
```
fetch_observation_data_from_id
fetch_observation_data
fetch_ground_station_data
fetch_satellite_data
fetch_tle_of_observation
fetch_telemetry
fetch_transmitters
fetch_satellites
post_telemetry
```

Variables:
```
NETWORK_DEV_BASE_URL
NETWORK_BASE_URL
DB_DEV_BASE_URL
DB_BASE_URL
```

## History
* 2018-06-13: Creation of python module `satnogs_api_client.py`
  at <https://gitlab.com/kerel-fs/satnogs-ARISS_contact_map/-/commit/fee38557dbaa356204d988ea51da6593fb60c4c8>
* 2018-10-27: Initial commit in python-satnogs-api repo.
  Code imported from satnogs-decoders, which in-turn imported
  from kerels ARISS contact map repo.
  <https://gitlab.com/kerel-fs/satnogs-ARISS_contact_map>
* 2018-12-07: The project was moved from kerel to lsf fork:
  <https://gitlab.com/librespacefoundation/satnogs/satnogs-decoders/-/issues/6#note_122999437>
* some point before 2022: LSF fork was archived.
* 2022-03-19: Project moved/restarted to kerel again, first release done.

## License

GNU AGPLv3
