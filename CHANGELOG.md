# Changelog

## 0.3 - 2024-01-19

- Add Python 3.12 support ([python-satnogs-api#5](https://gitlab.com/kerel-fs/python-satnogs-api/-/merge_requests/5)).

## 0.2 - 2022-12-23

- Fixed `fetch_tle_of_observation` for new SatNOGS Network release ([python-satnogs-api#3](https://gitlab.com/kerel-fs/python-satnogs-api/-/issues/3)).

## 0.1 - 2022-03-19

- First release of `satnogs_api_client`
