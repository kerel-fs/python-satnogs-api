# flake8: noqa
"""
SatNOGS API client module initialization
"""
from __future__ import absolute_import

from .satnogs_api_client import fetch_observation_data_from_id, \
                                fetch_observation_data, \
                                fetch_ground_station_data, \
                                fetch_satellite_data, \
                                fetch_tle_of_observation, \
                                fetch_telemetry, \
                                fetch_transmitters, \
                                fetch_satellites, \
                                get_paginated_endpoint, \
                                post_telemetry, \
                                NETWORK_DEV_BASE_URL, \
                                NETWORK_BASE_URL, \
                                DB_DEV_BASE_URL, \
                                DB_BASE_URL


from . import _version
__version__ = _version.get_versions()['version']
