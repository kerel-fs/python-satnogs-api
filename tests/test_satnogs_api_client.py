from satnogs_api_client import fetch_satellites, DB_BASE_URL


def test_fetch_satellites():
    '''
    Check fetch_satellites against the online SatNOGS DB API.

    Test requires an internet connection.
    '''

    # Fetch the satellites of interest from satnogs-db
    sats = fetch_satellites(url=DB_BASE_URL, max_entries=None)

    # Rudimentary check of the result by only looking at the first satellite
    assert(all(key in sats[0].keys() for key in ['norad_cat_id', 'status']))
